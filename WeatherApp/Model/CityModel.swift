//
//  CityModel.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 17.10.2021.
//

import Foundation

struct CityModel {
    var mainImage: String = "buildings"
    var cityName: String = "Name" // second vc
    var date: String = ""
    var temperature: Int = 0// second vc
    var conditionWeatherImage: String = ""// second vc
    var condition: String = ""// second vc
    var url: String = ""
    
    init?(weatherData: WeatherData) {
        mainImage = "buildings"
        date = currentDate()
        temperature = weatherData.fact.temp
        conditionWeatherImage = weatherData.fact.icon
        condition = weatherData.fact.condition
        url = weatherData.info.url
    }
    
    init() {
        
    }
}

func currentDate() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "dd.MM.yyyy"
    let result = formatter.string(from: date)
    return result
}
