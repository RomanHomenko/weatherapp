//
//  NetworkWeatherManager.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 21.10.2021.
//

import Foundation

struct NetworkWeatherManager {
    
    // work with yandexWeather API
    func fetchWeather(latitude: Double, longitude: Double, completionHandler: @escaping(CityModel) -> Void) {
        let urlString = "https://api.weather.yandex.ru/v2/forecast?lat=\(latitude)&lon=\(longitude)"
        guard let url = URL(string: urlString) else { return }

        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.addValue(Constants.apiKey, forHTTPHeaderField: "X-Yandex-API-Key")
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
//            print(String(data: data, encoding: .utf8)!)
            if let weather = self.parsJson(withData: data) {
                completionHandler(weather)
//                print(weather)
            }
        }
        task.resume()
    }
    
    func parsJson(withData data: Data) -> CityModel? {
        let decoder = JSONDecoder()
        
        do {
            let weatherData = try decoder.decode(WeatherData.self, from: data)
            guard let weather = CityModel(weatherData: weatherData) else {
                return nil
            }
            return weather
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
