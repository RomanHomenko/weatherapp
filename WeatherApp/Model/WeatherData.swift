//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 21.10.2021.
//

import Foundation

struct WeatherData: Decodable {
    
    let info: Info
    let fact: Fact
    let forecasts: [Forecast]
}

struct Info: Decodable {
    let url: String
}

struct Fact: Decodable {
    let temp: Int
    let icon: String
    let condition: String

//    MARK: add your own icons for this cases
//    case bknN = "bkn_n"
//    case ovc = "ovc"
//    case ovcRa = "ovc_-ra"
//    case ovcRaSn = "ovc_ra_sn"
//    case ovcSn = "ovc_-sn"
//    case skcN = "skc_n"
}

struct Forecast: Decodable {
    let parts: Parts
}

struct Parts: Decodable {
    let day: Day
}

struct Day: Decodable {
    let tempMin: Int?
    let tempMax: Int?
    
    enum CodingKeys: String, CodingKey {
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
}
