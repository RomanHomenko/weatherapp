//
//  Constants.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 18.10.2021.
//

import Foundation
import UIKit

struct Constants {
    static let leftDistanceToView: CGFloat = 40
    static let rightDistanceToView: CGFloat = 40
    static let cityMinimumLineSpacing: CGFloat = 40
    static let cityItemWidth = (UIScreen.main.bounds.width - Constants.leftDistanceToView - Constants.rightDistanceToView)
    static let apiKey = "f60c8467-4d27-4aa5-90ea-3f46059e3cde"
}
