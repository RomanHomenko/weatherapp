//
//  CityCollectionViewCell.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 17.10.2021.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {
    static let reuseID = "CityCollectionViewCell"
    
    let cityImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .gray
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let nameCityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var dateLabel: UILabel = {
        let label = UILabel() // learn about it later
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var temperatureLabel: UILabel = {
        let label = UILabel() // learn about it later
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let weatherImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setImageConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(city: CityModel) {
        
    }
    
    func setImageConstraint() {
        addSubview(cityImageView)
        cityImageView.addSubview(weatherImage)
//        addSubview(weatherImage)
        addSubview(nameCityLabel)
        addSubview(dateLabel)
        addSubview(temperatureLabel)
        
        cityImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        cityImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        cityImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        cityImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1/1.7).isActive = true
        
        weatherImage.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        weatherImage.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3).isActive = true
        weatherImage.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        weatherImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
        weatherImage.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1/5).isActive = true
        
        nameCityLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        nameCityLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        nameCityLabel.topAnchor.constraint(equalTo: cityImageView.bottomAnchor, constant: 10).isActive = true
        
        dateLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        dateLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        dateLabel.topAnchor.constraint(equalTo: nameCityLabel.bottomAnchor, constant: 10).isActive = true
        
        temperatureLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        temperatureLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        temperatureLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10).isActive = true
        
    }
}

