//
//  CreateCityViewController.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 20.10.2021.
//

import UIKit

class CreateCityViewController: UIViewController {

    @IBOutlet weak var popCityView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        popCityView.layer.cornerRadius = 24
        self.view.backgroundColor = .black.withAlphaComponent(0.7)
        moveIn()
    }
    
    func moveIn() {
            self.view.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
            self.view.alpha = 0.0
            
            UIView.animate(withDuration: 0.24) {
                self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.view.alpha = 1.0
            }
        }
        
        func moveOut() {
            UIView.animate(withDuration: 0.24, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
                self.view.alpha = 0.0
            }) { _ in
                self.view.removeFromSuperview()
            }
        }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        moveOut()
    }

}
