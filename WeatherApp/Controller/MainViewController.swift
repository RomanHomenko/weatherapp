//
//  ViewController.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 16.10.2021.
//

// - TODO: Add new VC to create new City cell +
// - TODO: Add delegates to pass data between this VC and new VC
// - TODO: Add RealmSwift/CoreData to save current condition of app
// - TODO: Add function to button Change temperature from C to F and back
// - TODO: Add request from API weather to get current temperature for chosen city +
// - TODO: Add curent dateTIme to cell +

// - TODO: Add all cities in Russia to finder

import UIKit
//import CoreLocation

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var cityCollectionView: UICollectionView!
    
//    var cells = [CityModel]()
    let networkWeatherManager = NetworkWeatherManager()
    let getCityWeather = GetCityWeather()
    
    let emptyCytiy = CityModel() // solve this later!!!
    var citiesArray = [CityModel]()
    let nameCitesArray = ["Омск", "Санкт-Петербург", "Тула", "Тюмень", "Новосибирск", "Челябинск", "Астана", "Уфа", "Пенза"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCollectionView()
        setConstraints()
        if citiesArray.isEmpty {
            citiesArray = Array(repeating: emptyCytiy, count: nameCitesArray.count)
        }
        addCities()
    }
    
    func addCities() {
        
        getCityWeather.getCityWeather(citiesArray: self.nameCitesArray) { index, weather in
            self.citiesArray[index] = weather
            self.citiesArray[index].cityName = self.nameCitesArray[index]
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        let popCityVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createCity") as! CreateCityViewController
        self.addChild(popCityVC)
        popCityVC.view.frame = self.view.frame
        self.view.addSubview(popCityVC.view)
        popCityVC.didMove(toParent: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return citiesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cityCollectionView.dequeueReusableCell(withReuseIdentifier: CityCollectionViewCell.reuseID, for: indexPath) as! CityCollectionViewCell
        setCell(cell)
        cell.cityImageView.image = UIImage(named: citiesArray[indexPath.item].mainImage)
        cell.dateLabel.text = citiesArray[indexPath.item].date
        cell.temperatureLabel.text = String(citiesArray[indexPath.item].temperature)
        cell.nameCityLabel.text = citiesArray[indexPath.item].cityName
        cell.weatherImage.image = UIImage(named: citiesArray[indexPath.item].conditionWeatherImage)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constants.cityItemWidth, height: cityCollectionView.frame.height * 0.9)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let popCityVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "city") as! CityViewController
        self.addChild(popCityVC)
        let cityWeather = citiesArray[indexPath.item]
        popCityVC.cityModel = cityWeather
        popCityVC.view.frame = self.view.frame
        self.view.addSubview(popCityVC.view)
        popCityVC.didMove(toParent: self)
    }
    
    func setCell(_ cell: CityCollectionViewCell) {
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.7
        cell.layer.shadowOffset = CGSize.zero
        cell.layer.shadowRadius = 5
        cell.backgroundColor = .systemGray6
    }
    
    
//    func set(cells: [CityModel]) {
//        self.cells = cells
//    }
    
    func setConstraints() {
        view.addSubview(cityCollectionView)

        cityCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        cityCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        cityCollectionView.topAnchor.constraint(equalTo: appNameLabel.bottomAnchor, constant: 15).isActive = true
        cityCollectionView.heightAnchor.constraint(equalToConstant: 650).isActive = true
    }
    
    func setCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        cityCollectionView.collectionViewLayout = layout
        cityCollectionView.delegate = self
        cityCollectionView.dataSource = self
        cityCollectionView.backgroundColor = .systemGray6
        cityCollectionView.register(CityCollectionViewCell.self, forCellWithReuseIdentifier: CityCollectionViewCell.reuseID)
        cityCollectionView.translatesAutoresizingMaskIntoConstraints = false
        layout.minimumLineSpacing = Constants.cityMinimumLineSpacing
        cityCollectionView.contentInset = UIEdgeInsets(top: 0, left: Constants.leftDistanceToView, bottom: 0, right: Constants.rightDistanceToView)
        cityCollectionView.showsHorizontalScrollIndicator = false
        cityCollectionView.showsVerticalScrollIndicator = false
    }
}
