//
//  CityViewController.swift
//  WeatherApp
//
//  Created by Роман Хоменко on 18.10.2021.
//

import UIKit

class CityViewController: UIViewController {

    @IBOutlet weak var popCityView: UIView!
    @IBOutlet weak var nameCityLabel: UILabel!
    @IBOutlet weak var conditionImage: UIView!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel! // normal temperature
    
    var cityModel: CityModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popCityView.layer.cornerRadius = 24
        self.view.backgroundColor = .black.withAlphaComponent(0.7)
        moveIn()
        refreshLabels()
    }
    
    func refreshLabels() {
        
        nameCityLabel.text = cityModel?.cityName
        
        
        // MARK: make your own array of icons and use conditionIconsName to set
//        let url = URL(string: "https://yastatic.net/weather/i/icons/funky/dark/\(cityModel!.conditionWeatherImage).svg")
//        let weatherImage = UIView(SVGURL: url!) { image in
//            image.resizeToFit(self.conditionImage.bounds)
//        }
//        conditionImage.addSubview(weatherImage)
        conditionLabel.text = cityModel?.condition
        tempLabel.text = String(describing: cityModel!.temperature)
    }
    
    func moveIn() {
            self.view.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
            self.view.alpha = 0.0
            
            UIView.animate(withDuration: 0.24) {
                self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.view.alpha = 1.0
            }
        }
        
        func moveOut() {
            UIView.animate(withDuration: 0.24, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
                self.view.alpha = 0.0
            }) { _ in
                self.view.removeFromSuperview()
            }
        }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        moveOut()
    }
}

